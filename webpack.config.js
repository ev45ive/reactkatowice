var path = require('path')
var ExtractText = require('extract-text-webpack-plugin')
var HTMLWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        bundle: './src/main.jsx',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js'
        //filename: 'bundle-[name]-[id]-[hash].js'
    },
    devtool: 'sourcemap',
    resolve: {
        extensions: ['.jsx', '.js']
    },
    module: {
        rules: [
            { test: /\.jsx?$/, use: 'babel-loader' },
            {
                test: /\.css$/, use: ExtractText.extract({
                    use: 'css-loader',
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.(woff|png|svg)$/, use: {
                    loader: 'file-loader', options: {
                        name: 'assets/[name].[hash].[ext]'
                    }
                }
            }
        ]
    },
    plugins: [
        new ExtractText('styles.css'),
        new HTMLWebpackPlugin({
            template: './src/index.html'
        }),
    ],
    devServer:{
        historyApiFallback: true
    }
}