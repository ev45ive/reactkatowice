import React from 'react'
import { fetchUsers } from '../reducers/users'
import { store } from '../store'
import {Todos} from '../containers/todos'

import {Route} from 'react-router-dom'

const UserListItem = props => <li onClick={props.onClick}
    className="list-group-item">
    {props.user.name}
</li>

export const Users = ({
    users = [],
    Item = UserListItem,
    history
}) => {
    return <div className="row">
        <div className="col">
            <h3> Users Tab </h3>
            <button onClick={() => store.dispatch(fetchUsers())}>Fetch</button>
            <ul className="list-group">
                {users.map(user =>
                    <Item key={user.id} user={user} onClick={
                        e => history.push('/users/'+user.id)
                    }/>
                )}
            </ul>
        </div>
        <div className="col">
            <Route path="/users/:id" render={ props => <div>
                <div>{props.match.params.id}</div> 
                {/* <User id={props.match.params.id} /> */}
                <Todos userId={props.match.params.id} />
            </div>
            } />
        </div>

    </div>
}

// virtualDOOOOM!