import { createStore, combineReducers, applyMiddleware } from 'redux'
import {todos,initialTodos,addTodo,toggleTodo} from './reducers/todos'
import {users,initialUsers,fetchUsers} from './reducers/users'

const initialState = {
    todos: initialTodos,
    users: initialUsers
}

const rootReducer = combineReducers({
    todos,
    users,
    // data:combineReducers({
    //     users:users
    // })
})

// const rootReducer = (state, action) => {

//     switch(action.type){
//         case 'TEST':
//             return {
//                 ...state,
//                 test: Date.now()
//             }
//         default:
//         return {
//             ...state,
//             todos: todos(state.todos,action),,
//             users: users(state.users,action)
//         }
//     }
// }

const loggerMiddleware = store => next => action => {
    console.log('action', action, store.getState())

    let state = next(action)

    console.log('next state', state,store.getState())
    return state   
}
import {loadUsers} from './reducers/users'
const asyncMiddleware = store => next => action => {
    
    // // redux-thunk
    // if('function' === typeof action  ){
    //     return next(action(store.dispatch))
    // }

    // if( action instanceof Promise){

    // }

    if(action.type == 'FETCH_USERS'){
        fetch('http://localhost:3000/users')
        .then( resp => resp.json() )
        .then( users => {
            store.dispatch( loadUsers(users) )
        })
        return next(action)
    }
    
    return next(action)
}


export const store = createStore(rootReducer, initialState,applyMiddleware(
    loggerMiddleware,
    asyncMiddleware,
))

console.log([store,addTodo,toggleTodo])