import React from 'react'
import ReactDOM from 'react-dom'

import { Layout } from './components/Layout'
import {App} from './app'
import 'bootstrap/dist/css/bootstrap.css';
import './style.css';

import {store} from './store.js'
import {Provider} from 'react-redux'
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'

import { Todos } from './containers/todos'
import { Users } from './containers/users'

ReactDOM.render( 
<Router>
    <Provider store={store}>
        <Layout>
            <Switch>
                <Redirect path="/" exact to="/todos" />
                <Route path="/todos" component={Todos} />
                <Route path="/users" component={Users} />
            </Switch>
        </Layout>
    </Provider>
</Router>
, document.getElementById('app-root'))



