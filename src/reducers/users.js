
export const initialUsers = {
    order:[],
    index:{},
    lru:[],
    pagenum:1,
    perpage:10,
    page:[]

}

const FETCH_USERS = 'FETCH_USERS'
const LOADED_USERS = 'LOADED_USERS'

export const loadUsers = (users) => {
    return {
        type: LOADED_USERS,
        payload:{
            users,
            pending:false
        }
    }
}

export const fetchUsers = () => {
    
    return {
        type: FETCH_USERS,
        payload:{
            pending:true,
        }
    }
  
}
export const users = (state = initialUsers ,action) => {
    switch(action.type){
        case FETCH_USERS:
            return {
                ...state,
            }
        case LOADED_USERS:
            const users = action.payload.users;
            return {
                ...state,
                order: users.map(user => user.id),
                index: users.reduce( (index, user)=>{
                    index[user.id] = user
                    return index;
                },{...state.index})
            }
        default:
            return state
    }
}