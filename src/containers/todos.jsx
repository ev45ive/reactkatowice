import {Todos as TodosComponent} from '../components/todos'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {addTodo,toggleTodo} from '../reducers/todos.js'
import {createSelector} from 'reselect'

// const getTodosList = state => (
//     state.todos.order.map( id => state.todos.index[id])
// )

const getTodosList = createSelector([
    state => state.order,
    state => state.index
], (order,index)=> order.map( id => index[id]))

const connector = connect(
    state => ({
        todos: getTodosList(state.todos)
    }),
    dispatch => bindActionCreators({
        onAdd: addTodo,
        onToggle: toggleTodo
    },dispatch),
    // (state, dispatch, props)=>{
    //     return {
    //         todos
    //     }
    // }
)

export const Todos = connector(TodosComponent)












// export const connect = (Component, mapStateToProps, mapActionsToProps ) => {
//     const Container = (props,context) => {
//         stateProps = mapStateToProps(context.store)
//         actionProps = mapActionsToProps(context.actions)

//         return <Component {...props} {...stateProps} {...actionProps} />
//     }
//     Container.contextTypes = {
//         store: propTypes.object,
//         actions: propTypes.object
//     }

//     return Container;
// }

// export const Todos(TodosComponent, 
//     ({todos}) => ({todos}), 
//     ({addTodo}) => ({ addTodo})
// )


/*

export const Todos = (props,{store,actions}) => {
    debugger;
    actions = bindActionCreators(actions, a=> store.dispatch(a))
    const todos = store.getState().todos;

    const list = todos.order.map( id => todos.index[id]);

    return <Component {...props} todos={list}
        onAdd={actions.addTodo}
        onToggle={actions.toggleTodo}  />
}

Todos.contextTypes = {
    store: propTypes.object,
    actions: propTypes.object
}*/