import {connect} from 'react-redux'
import {Users as UsersComponent } from '../components/users'
import {fetchUsers} from '../reducers/users'


import {createSelector} from 'reselect'

const getUsersList = createSelector([
    state => state.order,
    state => state.index
], (order,index)=> order.map( id => index[id]))


export const Users = connect(
    state => ({
        users: getUsersList(state.users)
    }),
    dispatch => ({
        fetch: ()=>dispatch(fetchUsers())
    })
)(UsersComponent)