import React from 'react'

import { Todos } from './containers/todos'
import { Users } from './containers/users'
import propTypes from 'prop-types'
import {Tree} from './components/tree'

const UserListMagicalItem = props =>  <li 
    className="list-group-item">
    {props.user.name} *
</li>


export class App extends React.Component {

    constructor(){
        super()
        this.state = {
            tab: 'todos'
        }
    }

    // static childContextTypes = {
    //     store: propTypes.object,
    //     actions: propTypes.object
    // }

    // getChildContext(){
    //     return {
    //         store: store,
    //         actions:{
    //             addTodo,toggleTodo
    //         }
    //     }
    // }


    setTab = (tab)=>{
        this.setState({
            tab
        })
    }

    render(){ 
        return <div>
            <div onClick={e => this.setTab('todos')}>Todos</div>
            <div onClick={e => this.setTab('users')}>Users</div>

            {this.state.tab == 'todos'? <div>
                <Todos />
            </div> :
            <div>  
                <Users Item={UserListMagicalItem}/>    
            </div> }

            {/*<Tree node={{id:1, name:'test',children:[{id:2, name:'test'}]}} /> */}

        </div>
    }
}