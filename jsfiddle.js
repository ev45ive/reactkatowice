


console.log(store,addTodo,toggleTodo);

var cache = {}
temp1.subscribe( () => {
 var todos = temp1.getState().todos;
 if(cache.todos != todos){
    console.log(  da) )
    cache.todos = todos;
 }
});
temp1.dispatch( temp2({title:'Test2',completed:true}) )
temp1.dispatch( temp3(34) )

///// GIT:
https://bitbucket.org/ev45ive/reactkatowice/downloads/

///// REDUX:
var inc = amount => ({type:'INC_COUNTER', amount});
var dec = amount => ({type:'DEC_COUNTER', amount});

var counterReducer = (state = 0 ,action) =>{
	switch(action.type){
        case 'INC_COUNTER': return state + action.amount 
        case 'DEC_COUNTER': return state - action.amount 
      }
}

[inc(1),inc(2),inc(3),dec(4)].reduce( (state, action) => { console.log(state, action)
   
   return { counter: counterReducer(state.counter, action) };
},{
  counter: 0
})


///// IMMUTABLE TOGGLE :
   toggleCompleted = (todoId) => {
        let todoIndex = this.state.todos.findIndex( todo => todo.id == todoId)
        const oldTodo = this.state.todos[todoIndex]
        const todo = {...oldTodo, completed: !oldTodo.completed}
        const todos = [...this.state.todos]
        todos.splice(todoIndex,1,todo)

        this.setState({
            todos
        })       
    }
///// Object SPREAD:
npm install --save-dev babel-plugin-transform-object-rest-spread"
.babelrc:
    "plugins": ["transform-class-properties","transform-object-rest-spread"]

///// JSX LISTS:
import React from 'react'
import ReactDOM from 'react-dom'

let counter = 0;
let myClass = 'test'

const Section = (props) => <div className={props.myClass} style={props.style}> 
        {props.text}
    </div>;

const list = []

setInterval(() => {
    counter++

    list.push('Element '+counter)

    const style = {
        border:'1px solid black',
    }

    const props = {
        style, myClass
    }

    ReactDOM.render(<div>
        {list.map( (item,index) => <Section key={item} {...props} text={item} />)}
    </div>, document.getElementById('app-root'))

}, 500)

///// JSX:
import React from 'react'
import ReactDOM from 'react-dom'

let counter = 0;
let myClass = 'test'

setInterval(() => {
    counter++

    const DIV = <div className={myClass} style={
        {
            border:'1px solid black',
            backgroundColor: counter %2 ==0? 'red':'green'
        }
    }> 
        Witaj w React! 
        <div>{counter}</div>
    </div>

    ReactDOM.render(DIV, document.getElementById('app-root'))

}, 500)



////// BABEL :
npm install --save-dev babel-cli babel-preset-es2015 babel-preset-react babel-plugin-transform-class-properties


/////// FETCH :
var request = (url) => fetch(url).then( resp => resp.ok? resp.json() : Promise.reject(resp.statusText) );

request('data.json').then( data => console.log(data) ) 
