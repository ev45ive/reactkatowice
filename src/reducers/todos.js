export const initialTodos = {
        order: [23,34],
        index: {
            23: {
                id: 23,
                title: 'Zakupy!',
                completed: true
            },
            34: {
                id: 34,
                title: 'Nauczyć się Reacta!', 
                completed: false
            },
        },
        newTitle: ''
    }
    
const ADDED_TODO = 'ADDED_TODO';
const TOGGLED_TODO = 'TOGGLED_TODO';

export const addTodo = (title) => {
    const newTodo = {
        id: Date.now(), 
        completed: false,
        title
    }
    return {
        type: ADDED_TODO,
        payload:{
            todo: newTodo
        }
    }
}

export const toggleTodo = (id) => {
    return {
        type: TOGGLED_TODO,
        payload:{
            id
        }
    }
}

export const todos = (state = initialTodos, action) => {
    switch(action.type){

        case ADDED_TODO:{
            const todo = action.payload.todo
            return {
                ...state, 
                index:{
                    ...state.index, 
                    [todo.id]:todo
                }, 
                order: [...state.order, todo.id]
            }
        }
        case TOGGLED_TODO:{
            const id = action.payload.id;
            const todo = state.index[id]
            return {
                ...state,
                index:{
                    ...state.index, 
                    [todo.id]:{
                        ...todo,
                        completed: !todo.completed
                    }
                }
            }
        }
        default:
            return state
    }
}