import React from 'react'
import {Link} from 'react-router-dom'

export const Layout = props => <div>
    <nav className="navbar navbar-light bg-faded">
        <div className="container">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" tabIndex="200" to="/todos">Todos</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" tabIndex="100" to="/users">Users</Link>
                </li>
            </ul>
        </div>
    </nav>
    <div className="container">
        <div className="row">
            <div className="col">
                {props.children}
            </div>
        </div>
    </div>
</div>;