import React from 'react'
import propTypes from 'prop-types'

const style ={
    completed: {
        textDecoration: 'line-through'
    }
}

export class Todos extends React.PureComponent{

    constructor(props, context){
        super()
        this.state={
            newTitle: ''
        }
        // console.log('Todos', context)
    }

    // static contextTypes = {
    //     store: propTypes.object
    // }

    static defaultProps = {
        onAdd: ()=>{},
        onToggle: ()=>{}
    }

    componentWillReceiveProps(...props){
        console.log(...props)
    }

    static propTypes = {
        todos: propTypes.arrayOf(propTypes.object).isRequired,
        onAdd: propTypes.func,
        onToggle: propTypes.func
    }

    titleChange = e => {
        this.setState({
            newTitle: e.target.value
        })
    }
    resetTitle = e =>{
        this.setState({
            newTitle: ''
        })
    }

    addTodo = (e) => {
        this.props.onAdd(this.state.newTitle)
        this.resetTitle()
    }

    render(){
        return <div> 
            <h3>Todos:</h3>
            <ul>
                { this.props.todos.map(todo => 
                <li onClick={ event => this.props.onToggle(todo.id) }
                    style={todo.completed? style.completed : {}} 
                    key={todo.id}>
                    {todo.title}
                </li>) }
            </ul>
            <div>
                <input  ref={ elem => elem && elem.focus() }
                        value={this.state.newTitle}
                        onChange={ this.titleChange }
                        onKeyUp={ e => e.key == 'Enter' && this.addTodo(e)} />
                                
                <button onClick={this.addTodo}>Dodaj</button>
            </div>
        </div>
    }
}