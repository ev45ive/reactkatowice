import React from 'react'
import {connect} from 'react-redux'

const initialTree = {
    nodes:{
        1: {
            id:1,
            name: 'root'
        },
        2: {
            id:2,
            name: 'child 1',
            parent: 1
        },
        3: {
            id:3,
            name: 'child 2',
            parent: 1
        },
        4: {
            id:4,
            name: 'child 2.1',
            parent: 3
        }
    },
    selected:0
}

const treeReducer = (state = initialTree, action)=>{
    switch(action.type){
        case 'SELECT_NODE':
            return {...state, selected: action.payload.id}
        default:
            return state
    }
}

export const TreeNode = ({node, select}) => 
<li onClick={e => select(node.id)}> {node.name} 
    {node.children && node.children.length? 
    <ul>
        {node.children.map(
            child => <TreeNode key={child.id} node={child} />
        )}        
    </ul> : null}
</li>

export const Tree = connect(
    state => ({

    }),
    dispatch => ({
        select: id => dispatch(select(id))
    }),
    (state,dispatch,props) => {
        let node = props.node
        return {
            ...state, ...dispatch, node 
        }
    }
)(TreeNode)